import Server from '../mixins/Server'

const state = {
  account: {
    expires: 'No expiration',
    payid: 'No pay ID',
    paydate: 'No pay date'
  },
  authorized: false,
  lastError: ''
}

const getters = {
  account_info: (state) => {
    return {
      expires: state.account.expires,
      payid: state.account.payid,
      paydate: state.account.paydate
    }
  },
  authorized: (state) => {
    return state.authorized
  }
}

const mutations = {
  account_set_info: (state, data) => {
    state.account.expires = data.expires
    state.account.paydate = data.paydate
    state.account.payid = data.payid
    state.authorized = true
  },
  account_unset_info: (state) => {
    state.account.expires = ''
    state.account.paydate = ''
    state.account.payid = ''
    state.authorized = false
    Server.deleteSession()
  },
  account_set_last_error: (state, data) => {
    state.lastError = data
  }
}

const actions = {
  refreshAccountInfo: (context) => {
    Server.getSubscription()
      .then(r => {
        context.commit('account_set_info', r.data)
    }).catch(e => {
      console.log(e)
      context.commit('account_set_last_error', 'There was an error when trying to refresh account information')
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
