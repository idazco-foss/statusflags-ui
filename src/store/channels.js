import Server from '../mixins/Server'

const state = {
  channelData: [],
  colorCycle: '',
  sseLastStateID: '',
  sseLastMsgTime: 0,
  refreshingAllData: false,
  serverOK: true
}

const getters = {
  channelNames: (state) => {
    const names = []
    for (const item of Object.entries(state.channelData)) {
      names.push(item[0])
    }
    return names
  },
  channelData: (state) => {
    return state.channelData
  },
  colorCycle: (state) => {
    return state.colorCycle
  },
  colorCyclesArray: (state) => {
    return state.colorCycle.split(',')
  },
  channelColor: (state, getters) => (name) => {
    if (state.channelData[name].type === 'color') {
      return getters.colorCyclesArray[state.channelData[name].val]
    }
    return false
  }
}

const mutations = {
  /*
    WARNING: Dont call this mutation outside this file!
    To change a channels value it must be changed on the server AND the state.
    Outside of this file, call the moveChannelValue action to change a channel's value.
   */
  setChannelValue: (state, data) => {
    state.channelData[data.name].val = data.val
  },
  setColorCycles: (state, data) => {
    state.colorCycle = data
  },
  setChannelLabelAndIcon(state, data) {
    if (!data.name) {
      return
    }
    state.channelData[data.name].label = data.label
    state.channelData[data.name].icon = data.icon
  },
  setAllChannels: (state, data) => {
    const chData = {}
    for (const [k, v] of Object.entries(data)) {
      // set unreserved channel names
      if (v.type !== Server.constants.channelTypes.reserved) {
        chData[k] = {
          name: k,
          type: v.type,
          val: parseInt(v.val),
          label: v.label,
          icon: v.icon
        }
      } else {
        // update store for reserved channels
        if (k === Server.constants.reservedChannelNames.colorCycle) {
          state.colorCycle = v.val
        }
      }
    }
    // update the store with regular channel data
    state.channelData = chData
  },
  setSseLastMsgTime: (state, data) => {
    state.sseLastMsgTime = data
    state.sseActivityOn = true
  },
  setSseLastStateID: (state, data) => {
    state.sseLastStateID = data
  },
  setRefreshingAllData_On: (state) => {
    state.refreshingAllData = true
  },
  setRefreshingAllData_Off: (state) => {
    state.refreshingAllData = false
  },
  setServerOK_false: (state) => {
    state.serverOK = false
  },
  setServerOK_true: (state) => {
    state.serverOK = true
  }
}

const actions = {
  /*
    Changes the value of the named channel up or down.
    If the channel is a color channel it will make sure that the value change stays within
    range of the available color cycle and automatically set it to zero if it exceeds its bounds.

    Payload:
    data = {
      name: // string ~ the name of the channel to change
      incr: // bool ~ if FALSE, then it will decrement the value else it will (default) increment it
    }
   */
  moveChannelValue: (context, data) => {
    const oldVal = context.state.channelData[data.name].val
    let newVal

    if (data.incr === false) {
      newVal = oldVal - 1
    } else {
      newVal = oldVal + 1
    }

    // Colors must not go beyond number of available colors
    if (context.state.channelData[data.name].type === 'color') {
      const max = context.getters.colorCyclesArray.length
      // Colors are an array which is zero indexed so check as >= instead of just >
      if (newVal >= max || newVal < 0) {
        newVal = 0
      }
    }

    // Update the Server, dont wait for SSE, just try refresh immediately.
    // Race conditions between the SSE refresh and this refresh is handled in state.
    Server.setChannel(data.name, newVal, null, null)
      .then(r => {
        if (r) {
          if (r.status !== 201 && r.status !== 200) {
            // todo: test this
            throw new Error('unexpected response for setChannel: ' + r.data.toString())
          }
          context.dispatch('refreshAllChannelData')
        }
      })
  },

  refreshAllChannelData: ({ commit, state }) => {
    // Race condition handler: if already refreshing data from another event then just return
    if (state.refreshingAllData) {
      return
    }
    commit('setRefreshingAllData_On')
    Server.getAllChannelData()
      .then(r => {
        if (state.sseLastStateID !== r.headers['domain-state']) {
          commit('setSseLastStateID', r.headers['domain-state'])
          commit('setAllChannels', r.data)
        }
        commit('setRefreshingAllData_Off')
    }).catch(e => {
        commit('setRefreshingAllData_Off')
        if (e.response.status === 401) {
          commit('account_unset_info')
          throw new Error('session expired')
        }
        throw new Error('failed to get data for all channels: ' + e.toString())
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}

