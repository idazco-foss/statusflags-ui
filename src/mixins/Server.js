/**
 * This file is exports wrappers for communicating with the server.
 *
 * NEVER may any of these exported wrappers update store data. If store data
 * needs to be updated then the code in the store must use these wrappers
 * to get server data then the store code can update itself.
 *
 * All the exported wrapper calls MUST return a Promise
 */

import axios from 'axios'
import { store } from '../store'
const SERVER_URL = 'https://server.statusflags.com'

const constants = {
  urlServerData: SERVER_URL + '/server',
  urlAccountData(path) {
    if (constants.acctDomain === '') {
      throw new Error('domain account is not set')
    }
    return SERVER_URL + '/acct/' + constants.acctDomain + path
  },
  channelTypes: {
    color: 'color',
    counter: 'counter',
    reserved: 'reserved'
  },
  reservedChannelNames: {
    colorCycle: 'vuex-state-colorCycleList'
  },
  cookie: {
    sessionID: 'sessionid'
  },
  acctDomain: location.hostname.split('.')[0]
}

// if going directly to login.statusflags.com then user needs to provide account domain
if (constants.acctDomain.toLowerCase() === 'login') {
  constants.acctDomain = ''
}

/*
Start SSE
 */
let sseClient = null
let sseTimeout = false

function sseRefreshChannelData() {
  // just return if Vuex is not initialized yet
  if (store === undefined) {
    return
  }

  // if refresh is already being done then just exit
  if (store.state.channels.refreshingAllData) {
    return
  }

  // only try refresh data if user is authorized
  if (!store.state.account.authorized) {
    console.log('sseRefreshChannelData() needs authorization')
  } else {
    try {
      store.dispatch('refreshAllChannelData')
    } catch (e) {
      console.log('dispatch to refreshAllChannelData in sseRefreshChannelData failed:', e)
    }
  }
}

function sseInit() {
  console.log('SSE init')
  sseClient = new EventSource(constants.urlAccountData('/events'))
  sseClient.onopen = function () { console.log('SSE open') }

  // SSE message
  sseClient.onmessage = function (event) {
    // we got a message from SSE - reset its timeout
    sseTimeoutInit()
    // record the time we got a message from SSE
    store.commit('setSseLastMsgTime', Date.now())
    // handle the message from SSE
    if (event.data.includes('state: ')) {
      const sseStateID = event.data.split(':')[1].trim()
      // only refresh channel data if we see a change in the data state
      if (store.state.channels.sseLastStateID !== sseStateID) {
        sseRefreshChannelData()
      }
    }
  }

  // SSE error
  sseClient.onerror = function(event) {
    if (event.target.readyState === EventSource.CLOSED) {
      console.log('SSE closed ' + '(' + event.target.readyState + ')')
    } else if (event.target.readyState === EventSource.CONNECTING) {
      console.log('SSE reconnecting ' + '(' + event.target.readyState + ')')
    }
  }

  // start the SSE timeout ...
  sseTimeoutInit()
  // then refresh data ONCE after 3 seconds refresh
  setTimeout(sseRefreshChannelData, 3000)
}

function sseTimeoutInit() {
  if (sseTimeout !== false) { clearTimeout(sseTimeout) }
  // SSE will automatically re-initialize after 15 seconds
  console.log('SSE keep-alive reset')
  sseTimeout = setTimeout(sseInit, 15000)
}


/** EXPORT SECTION: */

export default {
  constants: constants,
  sseClient: sseClient,
  sseInit: function() {
    sseInit()
  },

  /*
  Get an axios header object with the sessionID header already in it
   */
  sessionHeader: function () {
    const h = {}
    h[constants.cookie.sessionID] = this.getSessionID()
    return h
  },

  /*
  Update a channel
   */
  setChannel: function (name, value = null, label = null, icon = null) {
    if (name.toString().trim().length === 0) {
      throw new Error('Channel name is required')
    }
    if ((!value && value !== 0) && (!label && label !== '') && (!icon && icon !== '')) {
      throw new Error('Cant set channel with nothing')
    }

    const headers = this.sessionHeader()
    if (value || value === 0) {
      headers['Channel-Value'] = value.toString().trim()
    }
    if (label || label === '') {
      headers['Channel-Label'] = label.toString().trim()
    }
    if (icon || icon === '') {
      headers['Channel-Icon'] = icon.toString().trim()
    }
    return axios.put(constants.urlAccountData('/ch/') + name, null, {
      headers: headers
    }).catch(e => {
      if (e.response.status === 401) {
        this.sessionGone('Session expired')
      } else {
        throw new Error('setChannel failed: ' + e.message)
      }
    })
  },

  /*
  Create a new channel
   */
  makeChannel: function (name, type) {
    if (name.toString().trim().length === 0) {
      throw new Error('Channel name is required')
    }
    const headers = this.sessionHeader()
    headers['Channel-Value'] = 0
    headers['Channel-Type'] = type.toString().trim()
    return axios.put(constants.urlAccountData('/ch/') + name, null, {
      headers: headers
    }).catch(e => {
      if (e.response.status === 401) {
        this.sessionGone('Session expired')
      } else {
        throw new Error('setChannel failed: ' + e.message)
      }
    })
  },

  /*
  Updates the account password
   */
  setPassword: function (currPwd, newPwd) {
    const headers = this.sessionHeader()
    headers['Current-Password'] = currPwd
    headers['New-Password'] = newPwd
    return axios.put(constants.urlAccountData('/set-pwd'), null, {
      headers: headers
    })
  },

  /*
  Submits authentication info and gets account info (login)
   */
  doLogin: function(email, password) {
    let cfg = null
    // for user manual login:
    if (email.length && password.length) {
      cfg = {
        auth: { username: email, password: password },
        withCredentials: false
      }
    } else {
      throw new Error('No email, password or cookie for authentication')
    }
    return axios.get(constants.urlAccountData('/session'), cfg)
  },

  /*
   Return subscription information from the server
   */
  getSubscription: function() {
    return axios.get(constants.urlAccountData('/sub'), { headers: this.sessionHeader() })
  },

  /*
  Deletes channels from a string array of channel names
   */
  deleteChannels: function(channels) {
    const headers = this.sessionHeader()
    headers['Channel-Names'] = channels
    return axios.delete(constants.urlAccountData('/ch-list'), {
      headers: headers
    })
  },

  /*
  Gets all channel data for the domain
   */
  getAllChannelData: function() {
    return axios.get(constants.urlAccountData('/ch-all'), { headers: this.sessionHeader() })
  },

  /*
  Get server health
   */
  getHealth: function() {
    return axios.get(constants.urlServerData + '/health')
  },

  sessionGone: function(errMessage) {
    store.commit('account_set_last_error', errMessage)
    store.commit('account_unset_info')
  },

  setSessionID: function(value) {
    localStorage.sessionid = value
  },

  getSessionID: function() {
    return localStorage.sessionid
  },

  deleteSession: function() {
    localStorage.removeItem('sessionid')
  }
}
