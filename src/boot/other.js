import Vue from 'vue'

// Vue-Cookies
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)

// QIconPicker
import Plugin from '@quasar/quasar-ui-qiconpicker'
import '@quasar/quasar-ui-qiconpicker/dist/index.css'
Vue.use(Plugin)

