#!/usr/bin/env bash
cd "$(dirname "$0")"
quasar build
rm -rf ./public/*
mv ./dist/spa/* ./public/
git add ./public/*
git commit -m"update public SPA" ./public
